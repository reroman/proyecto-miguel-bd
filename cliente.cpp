#include "cliente.hpp"
#define FORMAT_DATE     "yyyy/MM/dd"


Cliente::Cliente( int _id ) throw( QString )
{
    setId( _id );
}

int Cliente::getId() const
{
    return id;
}

void Cliente::setId( int value ) throw( QString )
{
    if( value < 0 )
        throw QString( "El ID debe ser mayor a cero o igual a cero." );
    id = value;
}

QString Cliente::getNombre() const
{
    return nombre;
}

void Cliente::setNombre( const QString &value ) throw( QString )
{
    // Verifica que una cadena no sea puros espacios o esté vacía
    if( value.trimmed().isEmpty() )
        throw QString( "El nombre no puede estar vacío" );
    nombre = value.trimmed();
}

QString Cliente::getApPaterno() const
{
    return apPaterno;
}

void Cliente::setApPaterno( const QString &value ) throw( QString )
{
    if( value.trimmed().isEmpty() )
        throw QString( "El apellido paterno no puede estar vacío" );
    apPaterno = value.trimmed();
}

QString Cliente::getApMaterno() const
{
    return apMaterno;
}

void Cliente::setApMaterno( const QString &value ) throw( QString )
{
    if( value.trimmed().isEmpty() )
        throw QString( "El apellido materno no puede estar vacío" );
    apMaterno = value.trimmed();
}

QString Cliente::getFechaAlta() const
{
    return fechaAlta.toString( FORMAT_DATE );
}

void Cliente::setFechaAlta( const QString &value) throw( QString )
{
    QDate aux = QDate::fromString( value.trimmed(), FORMAT_DATE );

    if( !aux.isValid() )
        throw QString( "Fecha inválida. Verifica el formato (dd/mm/aaaa)." );
    fechaAlta = aux;
}

int Cliente::getCredito() const
{
    return credito;
}

void Cliente::setCredito( int value ) throw( QString )
{
    if( value < 0 )
        throw QString( "El crédito debe ser un entero mayor o igual a cero." );
    credito = value;
}

int Cliente::getDeuda() const
{
    return deuda;
}

void Cliente::setDeuda( int value ) throw( QString )
{
    if( value < 0 )
        throw QString( "La deuda debe ser un entero mayor o igual a cero." );
    deuda = value;
}

QString Cliente::at( int index ) throw( std::out_of_range )
{
    switch( index ){
    case 0:
        return QString::number( id );
    case 1:
        return nombre;
    case 2:
        return apPaterno;
    case 3:
        return apMaterno;
    case 4:
        return getFechaAlta();
    case 5:
        return QString::number( credito );
    case 6:
        return QString::number( deuda );
    default:
        throw std::out_of_range( "Indice fuera de rango." );
    }
}

void Cliente::at( int index, const QString &value ) throw( std::out_of_range, QString )
{
    bool ok;
    int aux;

    switch( index ){
    case 0:
        aux = value.toInt( &ok );
        if( !ok ) // Verfica que la conversión a entero sea exitosa
            throw QString("ID inválido.");
        setId( aux );
        break;
    case 1:
        setNombre( value );
        break;
    case 2:
        setApPaterno( value );
        break;
    case 3:
        setApMaterno( value );
        break;
    case 4:
        setFechaAlta( value );
        break;
    case 5:
        aux = value.toInt( &ok );
        if( !ok )
            throw QString( "El crédito debe ser un entero." );
        setCredito( aux );
        break;
    case 6:
        aux = value.toInt( &ok );
        if( !ok )
            throw QString( "La deuda debe ser un entero." );
        setDeuda( aux );
        break;
    default:
        throw std::out_of_range( "Indice fuera de rango." );
    }
}

QTextStream& operator <<( QTextStream &out, const Cliente &c )
{
    // Escribe cada dato del cliente separado por el caracter |
    out << c.id << '|' << c.nombre << '|' << c.apPaterno << '|'
        << c.apMaterno << '|' << c.fechaAlta.toString( FORMAT_DATE ) << '|'
        << c.credito << '|' << c.deuda << '\n';
    return out;
}

QTextStream& operator >>( QTextStream &in, Cliente &c ) throw( std::logic_error, QString )
{
    QString buffer = in.readLine();

    if( buffer.isEmpty() )
        throw std::logic_error( "Fin de archivo encontrado." );

    QStringList list = buffer.split( '|' ); // Parte toda la linea usando '|' como separador
    if( list.size() != Cliente::NUMERO_CAMPOS ) // Deben ser los n números de campos
        throw QString( "El archivo está dañado." );

    // Escribe los datos en el cliente
    for( int i = 0 ; i < Cliente::NUMERO_CAMPOS ; i++ )
        c.at( i, list.at(i) );

    return in;
}
