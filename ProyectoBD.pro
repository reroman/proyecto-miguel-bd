#-------------------------------------------------
#
# Project created by QtCreator 2015-02-08T00:48:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = ProyectoBD
TEMPLATE = app


SOURCES += main.cpp\
		mainwindow.cpp \
    cliente.cpp \
    clientdialog.cpp

HEADERS  += mainwindow.hpp \
    cliente.hpp \
    clientdialog.hpp

FORMS    += mainwindow.ui \
    clientdialog.ui
