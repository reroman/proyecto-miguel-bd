#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QCheckBox>
#include <QFile>
#include <array>
#include <list>
#include "cliente.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow( QWidget *parent = 0 );
    ~MainWindow();

public slots:
    void readFromFile();    // Lee los registros desde el archivo
    void writeToFile();     // Escribe los registros al archivo
    void refreshDataTable();// Escribe los datos en la GUI
    void addClient();       // Abre la ventana para un nuevo cliente
    void modifyClient();    // Abre la ventana con la información del cliente
    void deleteClient();    // Elimina un cliente

private slots:
    void on_checkboxesChanged(int); // Se ejecuta cuando se cambia el valor de algún checkbox
    void on_lineEdit_textChanged(const QString &arg1);  // Se ejecuta cuando se escribe en el campo buscar
    void on_tablaRegistros_customContextMenuRequested(const QPoint &pos); // Se ejecuta cuando dan clic derecho
    void on_tablaRegistros_itemSelectionChanged();  // Se ejecuta cuando cambia la selección de un registro

private:
    Ui::MainWindow *ui;
    std::array<QCheckBox*, Cliente::NUMERO_CAMPOS> checkboxes;
    std::list<Cliente> listaClientes;
    QFile archivo;
};

#endif // MAINWINDOW_HPP
