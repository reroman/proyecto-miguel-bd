#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <QString>
#include <QDate>
#include <QTextStream>

class Cliente
{
    friend QTextStream& operator <<( QTextStream&, const Cliente& ); // Escribe un cliente en archivo
    friend QTextStream& operator >>( QTextStream&, Cliente& ) throw( std::logic_error, QString ); // Lee un cliente desde archivo

public:
    static constexpr int NUMERO_CAMPOS = 7;


    Cliente( int _id = 0 ) throw( QString );

    int getId() const;
    void setId( int value ) throw( QString );

    QString getNombre() const;
    void setNombre( const QString &value ) throw( QString );

    QString getApPaterno() const;
    void setApPaterno( const QString &value ) throw( QString );

    QString getApMaterno() const;
    void setApMaterno( const QString &value ) throw( QString );

    QString getFechaAlta() const;
    void setFechaAlta( const QString &value ) throw( QString );

    int getCredito() const;
    void setCredito( int value ) throw( QString );

    int getDeuda() const;
    void setDeuda( int value ) throw( QString );

    /*
     *  Obtiene en forma de cadena cualquier dato, comenzando en el índice 0.
     * El orden de los datos es: id = 0, nombre = 1, apPaterno = 2, apMaterno = 3,
     * fechaAlta = 4, credito = 5, deuda = 6
     */
    QString at( int ) throw( std::out_of_range );

    /*
     * Establece algún valor a partir de una cadena. El orden es el mismo que el
     * anterior
     */
    void at( int, const QString& ) throw( std::out_of_range, QString );


private:
    int id;
    QString nombre;
    QString apPaterno;
    QString apMaterno;
    QDate fechaAlta;
    int credito;
    int deuda;
};

#endif // CLIENTE_HPP
