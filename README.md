# PROYECTO PARCIAL 1 #

## Planteamiento ##

1. Por equipos de máximo 4 personas, hacer una aplicación que permita almacenar registros de clientes con los siguientes datos:
> * IdCliente
> * Nombre
> * ApPaterno
> * ApMaterno
> * FechaAlta (AAAA/MM/DD)
> * Crédito
> * Deuda

2. El uso de gestores de bases de datos esta prohibido y deben de hacerlo guardando la informacion en archivos de texto plano, que se puedan abrir desde el explorador de archivos para ver su contenido. Lo que su aplicación debe de hacer es:
> * Agregar un nuevo registro.
> * Modificar un dato de un registro en particular
> * Borrar un registro
> * Mostrar los regsitros almacenados permitiendo seleccionar las columnas de datos a mostrar con las siguientes opciones:
>> * Mostrar todos los registros
>> * Mostrar un registro en particular, especificado por el idcliente o nombre.
> * Al momento de registrar o modificar se debe validar lo siguiente:
>> * Que no se repita el IdCliente
>> * Que la fecha de alta cumpla con el formato indicado (4 digitos para el año, 2 para el mes y 2 para el dia), el valor de dias se encuentre entre 1 y 31 y el valor de mes entre 1 y 12
>> * Que el credito sea un entero mayor o igual a cero
>> * Que la deuda sea un entero mayor o igual a cero

3. Una vez que lo terminen respondan las siguientes preguntas, en las respuestas deben incluir lo que se necesita y la idea general de la solución.
> * ¿Qué tendrían que hacer para permitir cambiar el orden de los datos en diferentes consultas? Por ejemplo que en una el orden fuera (nombre, apPaterno,…) y en otra (ApPaterno, nombre,…).
> * ¿Cómo le harían para mostrar datos o información generados a partir de otros campos?, por ejemplo: nombre completo= nombre + apPaterno+ ApMaterno, Mensualidad a 6 meses=Deuda/6
> * Que tan fácil/difícil seria la agregación de un nuevo dato, por ejemplo teléfono, mail. ¿Por qué? ¿Qué tendrían que hacer?¿Afectaria sus funciones ya desarrolladas?

## Dependencias ##
* Qt5 [[página de descarga](http://www.qt.io/download/)]

## Compilación y Ejecución ##
```sh
$ git clone https://reroman@bitbucket.org/reroman/proyecto-miguel-bd.git 
$ cd proyecto-miguel-bd
$ qmake
$ make
$ ./ProyectoBD
```

## Licencia ##
GPL v3

## Autor ##
Ricardo Román <[reroman4@gmail.com](maito:reroman4@gmail.com)\>
