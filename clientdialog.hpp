#ifndef CLIENTDIALOG_HPP
#define CLIENTDIALOG_HPP

#include <QDialog>
#include <QLineEdit>
#include <list>
#include <array>
#include "cliente.hpp"

namespace Ui {
class ClientDialog;
}

class ClientDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ClientDialog( std::list<Cliente>&, Cliente* = NULL, QWidget* = NULL );
    ~ClientDialog();

public slots:
    // Función para leer los datos del cliente pasado y establecerlos en los
    // campos de texto
    void readFromClient();

    // Función para establecer los datos de la GUI en un nuevo Cliente
    // retorna true en caso de exito, false en caso contrario
    bool writeToClient();

private slots:
    void on_buttonBox_accepted(); // Función que se ejecuta cuando se presiona Ok

private:
    Ui::ClientDialog *ui;
    std::list<Cliente> &listaCliente;
    Cliente *cliente;
    std::array<QLineEdit*, Cliente::NUMERO_CAMPOS> linesEdit;
};

#endif // CLIENTDIALOG_HPP
