#include "clientdialog.hpp"
#include <QMessageBox>
#include "ui_clientdialog.h"

ClientDialog::ClientDialog(std::list<Cliente> &clientes, Cliente *c, QWidget *parent ) :
    QDialog( parent ),
    ui( new Ui::ClientDialog ),
    listaCliente( clientes ),
    cliente( c )
{
    ui->setupUi( this );
    setFixedSize( size() ); // Establece un tamaño fijo para el diálogo

	// Agrega todos los campos de texto a un arreglo para que sea más facil
	// escribir y leer la información
    linesEdit[0] = ui->lineId;
    linesEdit[1] = ui->lineNombre;
    linesEdit[2] = ui->lineApPaterno;
    linesEdit[3] = ui->lineApMaterno;
    linesEdit[4] = ui->lineFechaAlta;
    linesEdit[5] = ui->lineCredito;
    linesEdit[6] = ui->lineDeuda;

    if( cliente )   // Si un cliente es pasado establece sus datos en la GUI
        readFromClient();
}

ClientDialog::~ClientDialog()
{
    delete ui;
}

void ClientDialog::readFromClient()
{
    // Establece en cada campo de texto los datos del cliente
    for( int i = 0 ; i < Cliente::NUMERO_CAMPOS ; i++ )
        linesEdit.at( i )->setText( cliente->at(i) );
}

bool ClientDialog::writeToClient()
{
    Cliente nuevo;

    try{
        // Obtiene los datos desde la GUI y los establece en 'nuevo'
        for( int i = 0 ; i < Cliente::NUMERO_CAMPOS ; i++ )
            nuevo.at( i, linesEdit.at(i)->text() );

        for( Cliente &c : listaCliente )
            if( c.getId() == nuevo.getId() && &c != cliente ) // Verifica que el ID no exista en otro Cliente
                throw QString( "Ya existe otro cliente con el mismo ID." );
    }
    catch( QString &e ){
        QMessageBox::critical( this, "Datos inválidos", e );
        return false;
    }
    if( cliente ) // Si un cliente fue pasado al constructor se sobreescriben sus valores
        *cliente = nuevo;
    else // Si es un cliente nuevo lo agrega a la lista de clientes
        listaCliente.push_back( nuevo );
    return true;
}

void ClientDialog::on_buttonBox_accepted()
{
    if( writeToClient() )
        accept();   // Cierra el diálogo y lo marca como Aceptado
}
