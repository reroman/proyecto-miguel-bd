#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "clientdialog.hpp"
#include <QTextStream>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Metemos los checkboxes de la GUI en un array
    checkboxes[0] = ui->checkID;
    checkboxes[1] = ui->checkNombre;
    checkboxes[2] = ui->checkApPaterno;
    checkboxes[3] = ui->checkApMaterno;
    checkboxes[4] = ui->checkFechaAlta;
    checkboxes[5] = ui->checkCredito;
    checkboxes[6] = ui->checkDeuda;

    // A cada checkbox del array le asociaciamos la función on_checkboxesChanged
    for( QCheckBox *i : checkboxes )
        connect( i, SIGNAL( stateChanged(int) ), SLOT( on_checkboxesChanged(int) ) );

    // Asocia el clic del boton agregar y la opción agregar a la función addClient
    connect( ui->btnAgregar, SIGNAL(clicked()), SLOT(addClient()) );
    connect( ui->action_Agregar, SIGNAL(triggered()), SLOT(addClient()) );

    // Asocia la opcion modificar del menú y el doble clic de un registro a la función modifyClient
    connect( ui->action_Modificar, SIGNAL(triggered()), SLOT(modifyClient()) );
    connect( ui->tablaRegistros, SIGNAL(doubleClicked(QModelIndex)), SLOT(modifyClient()) );

    // Asocia la opcion eliminar del menú con la función deleteClient
    connect( ui->action_Eliminar, SIGNAL(triggered()), SLOT(deleteClient()) );
    connect( ui->actionAcerca_de_Qt, SIGNAL(triggered()), qApp, SLOT(aboutQt()) );

    archivo.setFileName( "registros.dat" );
    if( archivo.exists() ) // Si el archivo registros.dat existe lee su contenido
        readFromFile();
}

MainWindow::~MainWindow()
{
    // Elimina cada celda de a tabla
    for( int i = 0 ; i < ui->tablaRegistros->rowCount() ; i++ )
        for( int j = 0 ; j < ui->tablaRegistros->columnCount() ; j++ )
            delete ui->tablaRegistros->item( i, j );
    delete ui; // Elimina la GUI
}

void MainWindow::on_checkboxesChanged(int state) // Satate es el nuevo valor del checkbox
{
    unsigned int column;
    QObject *sender = QObject::sender(); // Se obtiene la fuente del evento

    // Recorre los checkbox para saber quién provocó el evento
    for( column = 0 ; column < checkboxes.size() ; column++ )
        if( checkboxes.at( column ) == sender ){
            if( state ) // Si fue marcado muestra su columna
                ui->tablaRegistros->showColumn( column );
            else // Si no la oculta
                ui->tablaRegistros->hideColumn( column );
        }
}

void MainWindow::readFromFile()
{
    archivo.open( QIODevice::ReadOnly | QIODevice::Text );
    QTextStream in( &archivo );
    Cliente cliente;

    while( !in.atEnd() ){
        try{
            // Lee un cliente y lo agrega a la lista de clientes
            in >> cliente; // LLama a la función operator >> (TextStream, Cliente) en cliente.cpp
            listaClientes.push_back( cliente );
        }
        catch( QString &e ){
            QMessageBox::warning( this, "Error al leer archivo", "El archivo de registros está dañado. Se creará uno nuevo." );
            archivo.close();
            listaClientes.clear();
            return;
        }
        catch( std::logic_error& ){}
    }
    archivo.close();
    refreshDataTable(); // Actualiza la GUI con los nuevos clientes
}

void MainWindow::writeToFile()
{
    archivo.open( QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text );

    if( !archivo.isOpen() ){
        QMessageBox::critical( this, "Error al crear archivo", "No se puede crear el archivo registros.dat" );
        return;
    }

    QTextStream out( &archivo );
    for( Cliente c : listaClientes ) // Escribe cada cliente de la lista en el archivo
        out << c; // Llama a la función operator <<( TextStream, Cliente ) en cliente.cpp
    archivo.close();
}

void MainWindow::refreshDataTable()
{
    unsigned int numRows = ui->tablaRegistros->rowCount();

    // Ordena los clientes usando su número de ID
    listaClientes.sort( []( const Cliente &c1, const Cliente &c2){
                                return c1.getId() < c2.getId();
    } );

    // Si el número de clientes es mayor a los que hay en la tabla
    // agrega las filas necesarias con sus respectivos items para cada celda
    if( numRows < listaClientes.size() )
        for( ; numRows < listaClientes.size() ; numRows++ ){
            ui->tablaRegistros->setRowCount( numRows + 1 );
            for( int i = 0 ; i < Cliente::NUMERO_CAMPOS ; i++ ){
                QTableWidgetItem *item = new QTableWidgetItem();
                item->setTextAlignment( Qt::AlignHCenter | Qt::AlignVCenter );
                ui->tablaRegistros->setItem( numRows, i, item );
            }
        }
    // Si el numero de clientes es menor a los que hay en la tabla
    // elimina las filas sobrantes con sus respectivos items
    else if( numRows > listaClientes.size() )
        for( ; numRows > listaClientes.size() ; numRows-- ){
            for( int i = 0 ; i < Cliente::NUMERO_CAMPOS ; i++ )
                delete ui->tablaRegistros->item( numRows - 1, i );
            ui->tablaRegistros->setRowCount( numRows - 1 );
        }

    // Escribe los datos de cada cliente en la tabla
    auto c = listaClientes.begin();
    for( unsigned int i = 0 ; i < numRows ; c++, i++ )
        for( int j = 0 ; j < Cliente::NUMERO_CAMPOS ; j++ )
            ui->tablaRegistros->item( i, j )->setText( c->at(j) );
}

void MainWindow::addClient()
{
    ClientDialog dialogo( listaClientes, NULL, this );

    // Actualiza la GUI sólo si presionó Ok en la ventana Cliente
    if( dialogo.exec() == QDialog::Accepted ){
        refreshDataTable();
        writeToFile();
        ui->lineEdit->clear();
    }
}

void MainWindow::modifyClient()
{
    int i = 0;
    int row = ui->tablaRegistros->currentRow(); // Obtenemos la fila del cliente seleccionado
    auto cliente = listaClientes.begin();

    // Contamos hasta encontrar el cliente en la lista
    while( i < row ){
        i++;
        cliente++;
    }
    // Se crea un diálogo con el cliente seleccionado
    ClientDialog dialogo( listaClientes, &(*cliente), this );
    if( dialogo.exec() == QDialog::Accepted ){ // Actualiza la GUI si se presionó Ok
        refreshDataTable();
        writeToFile();
        emit ui->lineEdit->textChanged( ui->lineEdit->text() ); // Emitimos una señal por si había algo en el filtro
    }                                                           // de búsqueda para que vuelva a filtrar los registros
}

void MainWindow::deleteClient()
{
    int i = 0;
    int row = ui->tablaRegistros->currentRow(); // Obtenermos la fila del cliente seleccionado
    auto cliente = listaClientes.begin();

    // Recorremos la lista hasta encontrar el cliente en la posición i
    while( i < row ){
        i++;
        cliente++;
    }

    // Pedimos confirmación de eliminar
    if( QMessageBox::question( this, "Por favor confirme...",
                   "Estás seguro de eliminar el cliente " + cliente->at(0) ) == QMessageBox::Yes ){
        listaClientes.erase( cliente ); // Elimina el cliente de la lista
        refreshDataTable();             // Vuelve a actualizar los datos
        writeToFile();
        // Emitimos una señal para volver a aplicar el filtro si es que había uno
        emit ui->lineEdit->textChanged( ui->lineEdit->text() );
    }
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1) // arg1 es la cadena escrita en el campo Buscar
{
    unsigned int i;
    auto c = listaClientes.begin();

    // Si la cadena nueva está vacía muestra todos los clientes
    if( arg1.isEmpty() || arg1.isNull() )
        for( i = 0 ; i < listaClientes.size() ; i++ )
            ui->tablaRegistros->showRow( i );
    else
        for( i = 0 ; i < listaClientes.size() ; i++, c++ )
            if( c->at(0) == arg1 || // Concuerda con el ID?
                    c->getNombre().contains( arg1, Qt::CaseInsensitive ) || // el nombre contiene con lo buscado?
                    c->getApPaterno().contains( arg1, Qt::CaseInsensitive ) || // el apPaterno contiene con arg1?
                    c->getApMaterno().contains( arg1, Qt::CaseInsensitive ) )  // el apMaterno contiene con arg1?
                ui->tablaRegistros->showRow( i ); // Si sí, muestra el registro
            else
                ui->tablaRegistros->hideRow( i ); // Si no, lo oculta
}

void MainWindow::on_tablaRegistros_customContextMenuRequested(const QPoint &pos)
{
    // Muestra el menú en la parte donde se hizo el clic derecho
    ui->menu_Registro->popup( ui->tablaRegistros->viewport()->mapToGlobal( pos ) );
}

void MainWindow::on_tablaRegistros_itemSelectionChanged()
{
    // Si no hay registro seleccionado deshabilida la opción de modificar y eliminar
    if( ui->tablaRegistros->selectedItems().empty() ){
        ui->action_Eliminar->setEnabled( false );
        ui->action_Modificar->setEnabled( false );
    }
    else{ // Si sí lo hay, habilita las opciones
        ui->action_Eliminar->setEnabled( true );
        ui->action_Modificar->setEnabled( true );
    }
}
